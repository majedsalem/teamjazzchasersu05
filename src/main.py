from fastapi import FastAPI
from models.db import database, User, Stores


app = FastAPI(title="FastAPI, Docker")

@app.get("/")
async def read_root():
    return await User.objects.all()


@app.get("/stores")
async def read_root():
    return {"data" : await Stores.objects.all()}


@app.on_event("startup")
async def startup():
    if not database.is_connected:
        await database.connect()
    # create a dummy entry
    await User.objects.get_or_create(email="test@test.com")
    await Stores.objects.get_or_create(name="kungens kurva")
    await Stores.objects.get_or_create(name="stockholm gallerian")
    await Stores.objects.get_or_create(name="mall of scandinavia")



